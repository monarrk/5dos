#!/bin/sh

log() {
	printf "[CHAIN] %s\n" "$1"
}

log "Building mkbootimg"; {
	CWD="`pwd`"
	mkdir -p build
	cd build
	git clone https://gitlab.com/bztsrc/bootboot
	cd bootboot/mkbootimg

	log "Building with make"; {
		make
	}

	cp ./mkbootimg "${CWD}/mkbootimg"

	log "Built mkbootimg and copied to ${CWD}/mkbootimg"

	cd "${CWD}"
}
