#include "u.h"
#include "bootboot.h"
#include "text.h"
#include "env.h"
#include "string.h"

/* fonts compiled in */
volatile unsigned char *fonts[] = {
	&_binary_src_fonts_font_psf_start
};
/* idx of font to use */
int fontidx;

void init_text(void) {
	char font[ENVLEN];
	int r;

	if ((r = getenv(environment, "font", font))) {
		fontidx = chartoint(font[0]);
	} else fontidx = 0;

	textat(font, 0xffffff, 1, 1);
}

void textat(char *s, int color, int sx, int sy) {
	psf2_t *font = (psf2_t*)(fonts[fontidx]);
	int kx = sx, line, mask, offs;

	/* bytes per line */
	int bpl = (font->width + 7) / 8;

	/* iterate string */
	while (*s) {
		unsigned char *glyph = (unsigned char*)(fonts[fontidx]) + font->headersize +
			(*s > 0 && *s < font->numglyph ? *s : 0) * font->bytesperglyph;

		/* (row * height * pitch) + (column * (width + 1) * 4 */
		/* pitch is fb_scanline */
		offs = (sy * font->height * bootboot.fb_scanline) + (kx * (font->width + 1) * 4);
		for (int y = 0; y < font->height; y++) {
			line = offs;
			mask = 1 << (font->width - 1);
			for (int x = 0; x < font->width; x++) {
				/* draw the pixel */
				if ((int)*glyph & (mask)) {
					*((uint32_t*)((uint64_t)&fb + line)) = color;
				}
				mask >>= 1;
				line += 4;
			}
//			*((uint32_t*)((uint64_t)&fb+line)) = 0;
			glyph += bpl;
			offs += bootboot.fb_scanline;
		}
		s++;
		kx++;
	}
}

void clearat(int color, int sx, int sy) {
	psf2_t *font = (psf2_t*)(fonts[fontidx]);
	int line, mask, offs;

	offs = (sy * font->height * bootboot.fb_scanline) + (sx * (font->width + 1) * 4);
	for (int y = 0; y < font->height; y++) {
		line = offs;
		mask = 1 << (font->width - 1);
		for (int x = 0; x < font->width; x++) {
			/* draw the pixel */
			*((uint32_t*)((uint64_t)&fb + line)) = color;
			mask >>= 1;
			line += 4;
		}
		offs += bootboot.fb_scanline;
	}
}
