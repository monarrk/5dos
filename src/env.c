#include "u.h"
#include "bootboot.h"
#include "text.h"
#include "env.h"
#include "string.h"

int getenv(unsigned char *str, char *key, char *dest) {
	char name[ENVLEN];
	int namei = 0;
	char val[ENVLEN];
	int vali = 0;

	/* iterate the environment string */
	while (*str) {
		/* check if we've reached a value */
		if (*str == '=') {
			str++;
			/* get the value */
			while (*str) {
				if (*str == '\n') break;
				val[vali] = *str;
				vali++;
				str++;
			}
			val[vali] = '\0';
			name[namei] = '\0';
			
			/* is this the key? */
			if (strcmp(name, key) == 0) {
				strcpy(dest, val);
				return true;
			}

			namei = 0;
			vali = 0;
			for (int i = 0; i < ENVLEN; i++) {
				name[i] = 0;
				val[i] = 0;
			}
		} else if (*str == '\n') {
			namei = 0;
			vali = 0;
			for (int i = 0; i < ENVLEN; i++) {
				name[i] = 0;
				val[i] = 0;
			}
		} else {
			name[namei] = *str;
			namei++;
		}

		str++;
	}

	/* key not found */
	return false;
}
