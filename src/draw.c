#include "u.h"
#include "bootboot.h"
#include "draw.h"

/* draw a pixel to fb_default */
void drawpx(int x, int y, int color) {
	*((uint32_t*)(&fb + bootboot.fb_scanline*y + x*4)) = color;
}

/* clear the screen with color */
void clear(int color) {
	for (int x = 0; x < (bootboot.fb_width - 1); x++)
		for (int y = 0; y < (bootboot.fb_height - 1); y++)
			drawpx(x, y, color);
}

/* draw a rectangle */
void drawrect(int x, int y, int w, int h, int color) {
	for (; x < w; x++) {
		for (; y < h; y++) {
			drawpx(x, y, color);
		}
	}
}

/* draw a line on the x axis */
void drawxln(int x, int len, int color) {
	for (int y = x; y < len; y++) {
		drawpx(x, y, color);
	}
}
