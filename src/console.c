#include "u.h"
#include "console.h"
#include "text.h"

/* default console for the whole kernel */
console_t default_console;

/* initialize a console */
void init_console(int x, int y, int w, int h, int color, int bg) {
	default_console.ix = x;
	default_console.iy = y;
	default_console.x = x;
	default_console.y = y;
	default_console.w = w;
	default_console.h = h;
	default_console.color = color;
	default_console.bg = bg;
}

/* put a char in the console */
void console_putc(console_t *cons, char c) {
	switch (c) {
		/* create a newline */
		case '\n': {
			cons->y++;
			cons->x = cons->ix;
			break;
		}

		/* "tab" ahead 8 spaces */
		case '\t': {
			cons->x += 8;
			break;
		}

		default: {
			/* construct a string */
			/* TODO : probably a better idea to just make a 'charat' fn */
			char str[2];
			str[0] = c;
			str[1] = 0;
			/* TODO : this runs in parallel which makes it sometimes just overwrite the char. ugh. */
			/* clearat(cons->bg, cons->x++, cons->y); */
			textat(str, cons->color, cons->x++, cons->y);
			/* we're at the edge of the screen */
			if (cons->x >= cons->w) console_putc(cons, '\n');
		}
	}
}

/* print a string to the console */
void console_puts(console_t *cons, char *str) {
	while (*str) {
		console_putc(cons, *str);
		str++;
	}
}

/* change the color of a console */
void console_color(console_t *cons, int color) {
	cons->color = color;
}

/* helper fns for interacting with the default console */
void putc(char c) { console_putc(&default_console, c); }
void puts(char *s) { console_puts(&default_console, s); }
void color(int color) { console_color(&default_console, color); }
