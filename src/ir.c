#include "u.h"
#include "console.h"
#include "string.h"
#include "ir.h"

void machine_push(machine_t *machine, float val) {
	machine->stackptr++;
	machine->stack[machine->stackptr] = val;
}

float machine_pop(machine_t *machine) {
	float val;
	val = machine->stack[machine->stackptr--];
	return val;
}

void interpret_tape(float tape[]) {
	machine_t machine;

	machine.base = 0;
	machine.ptr = 0;
	machine.stackptr = 0;

	color(0x00aaff);
	for (; tape[machine.ptr]; machine.ptr++) {
		if (feq(tape[machine.ptr], PUSH)) {
			puts("PUSH\n");
			machine_push(&machine, tape[machine.ptr++]);
		} else if (feq(tape[machine.ptr], POP)) {
			puts("POP\n");
			machine_pop(&machine);
		} else if (feq(tape[machine.ptr], ADD)) {
			puts("ADD\n");
			float a = machine_pop(&machine);
			float b = machine_pop(&machine);
			machine_push(&machine, a + b);
		} else if (feq(tape[machine.ptr], SUB)) {
			puts("SUB\n");
			float a = machine_pop(&machine);
			float b = machine_pop(&machine);
			machine_push(&machine, a - b);
		} else if (feq(tape[machine.ptr], MUL)) {
			puts("MUL\n");
			float a = machine_pop(&machine);
			float b = machine_pop(&machine);
			machine_push(&machine, a * b);
		} else if (feq(tape[machine.ptr], DIV)) {
			puts("DIV\n");
			float a = machine_pop(&machine);
			float b = machine_pop(&machine);
			/* TODO : check for b == 0 */
			machine_push(&machine, a / b);
		} else if (feq(tape[machine.ptr], SIGN)) {
			puts("SIGN\n");
			float a = machine_pop(&machine);
			machine_push(&machine, a < 0 ? -1.0 : 1.0);
		} else if (feq(tape[machine.ptr], DBG)) {
			puts("DBG\n");
			color(0xff0000);
			for (int i = 0; i < machine.stackptr; i++) {
				char str[16];
				int_to_ascii((int)(machine.stack[i]), str);
				puts(str);
				puts("\n");
			}
			color(0x00aaff);
		} else {
			puts("UNKNOWN OP\n");
		}
	}
}

void interpret(char *str) {
	/* TODO */
	/* will likely require some form of dynamic memory, however */
}
