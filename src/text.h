typedef struct {
    uint32_t magic;
    uint32_t version;
    uint32_t headersize;
    uint32_t flags;
    uint32_t numglyph;
    uint32_t bytesperglyph;
    uint32_t height;
    uint32_t width;
    uint8_t glyphs;
} __attribute__((packed)) psf2_t;
extern volatile unsigned char _binary_src_fonts_font_psf_start;

void init_text(void);
void textat(char *s, int color, int sx, int sy);
void clearat(int color, int sx, int sy);
