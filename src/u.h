/* we don't assume stdint.h exists */
typedef char                int8_t;
typedef short int           int16_t;
typedef int                 int32_t;
typedef long int            int64_t;

typedef unsigned char       uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned long int   uint64_t;

#define false 0
#define true !(false)

#define feq(a, b) (((a) - (b)) == 0)
