#include "u.h"
#include "bootboot.h"
#include "text.h"
#include "draw.h"
#include "console.h"
#include "env.h"
#include "string.h"
#include "ir.h"

#define showval(prefix, value) \
	puts((prefix)); \
	color(0x00ffff); \
	puts((value)); \
	color(0xffffff); \
	puts("\n");

void _start() {
	char background[ENVLEN];
	char name[ENVLEN];
	char kernel[ENVLEN];
	char screen[ENVLEN];
	int res;
	int coreid;
	char cores[8];
	char strcoreid[32];
	char bspid[32];

	/* current coreid = (cpuid[eax=1].ebx >> 24) */
	asm volatile ("mov $0x1, %eax");
	asm volatile ("cpuid");
	asm volatile ("mov %%ebx, %0" : "=r" (coreid));

	/* only run if we're the correct core */
	/* in the future we could use this to create multithreaded stuff */
	if (coreid == bootboot.bspid) {
		while (1) ;
	} else {
		init_text();
		clear(0x111122);
	}

	/* get environment vars */
	res = getenv(environment, "background", background);
	res = getenv(environment, "name", name);
	res = getenv(environment, "kernel", kernel);
	res = getenv(environment, "screen", screen);

	/* set up the console */
	init_console(
			2, /* x margin of 2 ("initial x") */ 
			2, /* y margin of 2 ("initial y") */
			bootboot.fb_width - 1, /* width ("max x") */
			bootboot.fb_height - 1, /* height ("max y") */
			0xffffff, /* text color */
			0x111122 /* background color */
			);

	int_to_ascii(bootboot.numcores, cores);
	int_to_ascii(coreid, strcoreid);
	int_to_ascii(bootboot.bspid, bspid);

	puts("Welcome to 5DOS!\n\n");

	showval("name: ", name);
	showval("kernel: ", kernel);
	showval("screen: ", screen);
	showval("background: ", background);
	showval("cores: ", cores);
	showval("coreid: ", strcoreid);
	showval("BSP ID: ", bspid);

	color(0xff0000);

	color(0xffff00);
	puts("setup done!\n\n");

	/*
	color(0xffffff);
	puts("interpreting\n");
	float tape[] = {
		PUSH, 6.0f,
		PUSH, 5.0f,
		ADD,
		DBG
	};
	interpret_tape(tape);
	*/

	while(1);
}
