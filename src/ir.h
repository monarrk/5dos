/* TODO : make stack resizable? */
typedef struct {
	int len;
	int base;
	int ptr;
	int stackptr;
	float stack[64];
} machine_t;

/* vm instructions */
#define PUSH (1.0f)
#define POP (2.0f)
#define ADD (3.0f)
#define SUB (4.0f)
#define MUL (5.0f)
#define DIV (6.0f)
#define SIGN (7.0f)
#define DBG (8.0f)

/* NOTE TO SELF
 * comparisons can be done with SUB, because
 * if a - b == 0, a == b
 * if a - b < 0, a < b
 * if a - b > 0, a > b
 * so no CMP operator is needed
 */

void interpret_tape(float tape[]);
void interpret(char *str);
