int strcmp(char s1[], char s2[]);
void strcpy(char *dest, char *src);
void hex_to_ascii(int n, char str[]);
void int_to_ascii(int n, char str[]);
uint32_t strlen(const char *str);
void reverse(char s[]);
void append(char s[], char n);
int chartoint(char c);
