typedef struct {
	int ix;
	int iy;
	int x;
	int y;
	int w;
	int h;
	int color;
	int bg;
} console_t;

void init_console(int x, int y, int w, int h, int color, int bg);
void console_putc(console_t *cons, char c);
void console_puts(console_t *cons, char *str);
void console_color(console_t *cons, int color);

void putc(char c);
void puts(char *s);
void color(int color);
