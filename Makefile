CC = clang
CFLAGS = -target x86_64-none-elf -g -Wall -ffreestanding -fno-stack-protector -nostdinc -nostdlib -mno-red-zone
LDFLAGS =  -nostdlib -T sys/link.ld
STRIPFLAGS =  -s -K mmio -K fb -K bootboot -K environment -K initstack

KOBJ = \
      kernel.o \
      bootbootfont.o \
      string.o \
      text.o \
      draw.o \
      console.o \
      port.o \
      env.o \
      ir.o

all: img ${KOBJ} img/5DOS.elf 5DOS.tar.gz 5DOS.img

img:
	mkdir -p img

bootbootfont.o: src/fonts/font.psf
	objcopy -O elf64-x86-64 -B i386 -I binary src/fonts/font.psf ./bootbootfont.o

string.o: src/string.c src/string.h
	${CC} ${CFLAGS} -c src/string.c -o string.o

text.o: src/text.c src/text.h
	${CC} ${CFLAGS} -c src/text.c -o text.o

draw.o: src/draw.h src/draw.c
	${CC} ${CFLAGS} -c src/draw.c -o draw.o

console.o: src/console.c src/console.h
	${CC} ${CFLAGS} -c src/console.c -o console.o

port.o: src/port.c src/port.h
	${CC} ${CFLAGS} -c src/port.c -o port.o

env.o: src/env.c src/env.h
	${CC} ${CFLAGS} -c src/env.c -o env.o

ir.o: src/ir.c src/ir.h
	${CC} ${CFLAGS} -c src/ir.c -o ir.o

kernel.o: src/main.c
	${CC} ${CFLAGS} -c src/main.c -o kernel.o

img/5DOS.elf: ${KOBJ}
	ld.lld ${LDFLAGS} ${KOBJ} -o img/5DOS.elf

5DOS.tar.gz: config img/5DOS.elf
	tar czvf 5DOS.tar.gz img

5DOS.img: config img/5DOS.elf 5DOS.tar.gz sys/bootboot.json
	./mkbootimg sys/bootboot.json 5DOS.img

# make sure our code follows some certain rules
#verify:
#	# rule 1: no #includes in .h files
#	grep -rnw src/*.h -e "#include" | wc -l | [ ! -z ]

chain:
	sys/chain.sh

clean:
	rm -f ${KOBJ} *.o *.elf *.tar.gz *.img *.bin
	rm -rf img/

nuke: clean
	rm -rf build mkbootimg

run: all
	qemu-system-x86_64 \
		-serial stdio \
		-smp cores=2 \
		5DOS.img

dbg: all
	qemu-system-x86_64 \
		-S \
		-gdb tcp::1234 \
		-serial stdio \
		5DOS.img
